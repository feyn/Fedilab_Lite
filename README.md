[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# Fedilab Lite: A lightweight, multifunctional client for the Fediverse

### Mastodon • Pleroma • Peertube • GNU Social • Friendica • Pixelfed

**This lightweight version of Fedilab offers various privacy options and some of the most popular features of Fedilab**

**Built-in browser**

- Tracking protection
- Ad blocker
- Auto update block lists

**Privacy protection**

- Twitter URLs will be replaced with nitter.net
- YouTube URLs will be replaced with invidio.us
- Nitter and Invidious instances can be customized
- Option to clear cache when leaving the app


**Most popular features are still supported**

- Bookmarks
- Remember position
- Add notes
- Automatically split long statuses
- Translator
- Truncate long toots
- Scheduling
- Quick replies<br>*and more ...*


This version will work on Android 5.0 and above versions. App's download size is around 10Mb (with no minification). Also, default Art timeline and Peertube timeline have been removed, but it's still possible for you to create your own ones.

In bug reports, please mention that you're using the "Lite" version

## Donate

[![Backers on Open Collective](https://opencollective.com/mastalab/backers/badge.svg)](#backers) [![Sponsors on Open Collective](https://opencollective.com/mastalab/sponsors/badge.svg)](#sponsors)

[<img alt="Donate using Liberapay" src="https://img.shields.io/liberapay/patrons/tom79.svg?logo=liberapay"/>](https://liberapay.com/tom79/donate)

## Download
<!---
[<img alt='Get it on Google Play' src='https://framagit.org/tom79/fedilab/raw/develop/images/get-it-on-play.png' height="80"/>](#)
&nbsp;&nbsp;-->[<img alt='Get it on F-Droid' src='https://framagit.org/tom79/fedilab/raw/develop/images/get-it-on-fdroid.png' height="80"/>](https://f-droid.org/en/packages/app.fedilab.lite/)


## Resources

- [Wiki](https://fedilab.app/wiki/home/)

- [Releases & Changelogs](https://codeberg.org/tom79/Fedilab_Lite/releases)

<br>

Lead developer: [framapiaf.org/@fedilab](https://framapiaf.org/@fedilab)
<br>
Developer: [mastodon.social/@kasun](https://mastodon.social/@kasun)

## Backers

Thank you to all our backers! 🙏 [[Become a backer](https://opencollective.com/mastalab#backer)]

<a href="https://opencollective.com/mastalab#backers" target="_blank"><img src="https://opencollective.com/mastalab/backers.svg?width=890"></a>
## Sponsors
Support this project by becoming a sponsor. Your logo will show up here with a link to your website. [[Become a sponsor](https://opencollective.com/mastalab#sponsor)]
<a href="https://opencollective.com/mastalab/sponsor/0/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/0/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/1/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/1/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/2/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/2/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/3/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/3/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/4/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/4/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/5/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/5/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/6/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/6/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/7/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/7/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/8/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/8/avatar.svg"></a>
<a href="https://opencollective.com/mastalab/sponsor/9/website" target="_blank"><img src="https://opencollective.com/mastalab/sponsor/9/avatar.svg"></a>
