/* Copyright 2019 NickFreeman
 *
 * This file is a part of Fedilab
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * Fedilab is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with Fedilab; if not,
 * see <http://www.gnu.org/licenses>. */
package app.fedilab.lite.asynctasks;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import app.fedilab.lite.helper.Helper;
import app.fedilab.lite.sqlite.DomainBlockDAO;
import app.fedilab.lite.sqlite.Sqlite;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by NickFreeman on 11/05/2019.
 * Download the list of blocked tracking domains for build-in browser
 */

public class DownloadTrackingDomainsAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private WeakReference<Context> context;

    public DownloadTrackingDomainsAsyncTask(Context context) {
        this.context = new WeakReference<>(context);
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL("https://sebsauvage.net/hosts/hosts").openConnection();
            if (connection.getResponseCode() != 200)
                return false;
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            List<String> domains = new ArrayList<>();
            boolean canRecord = false;
            while ((line = br.readLine()) != null) {
                if (canRecord)
                    domains.add(line.replace("0.0.0.0 ", "").trim());
                else if (line.contains("# Blocked domains"))
                    canRecord = true;
            }
            br.close();
            connection.disconnect();

            SQLiteDatabase db = Sqlite.getInstance(context.get(), Sqlite.DB_NAME, null, Sqlite.DB_VERSION).open();
            new DomainBlockDAO(context.get(), db).set(domains);
            SharedPreferences sharedpreferences = context.get().getSharedPreferences(Helper.APP_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(Helper.TRACKING_LAST_UPDATE, Helper.dateToString(new Date()));
            editor.apply();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}